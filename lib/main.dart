import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
      title: "Flutter App",
      home: new Scaffold(
        appBar: new AppBar(title: new Text("Flutter Aplicación")),
        body: new Container(
          child: new Center(
            child: new Text("Texto dentro del container centrado"),
          ),
        ),
      )));
}
